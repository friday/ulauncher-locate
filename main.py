import logging
import subprocess
from ulauncher.api import Extension, Result
from ulauncher.api.shared.action.OpenAction import OpenAction
from ulauncher.api.shared.action.HideWindowAction import HideWindowAction

logger = logging.getLogger(__name__)
cmd = [
    'timeout', '5s',
    'ionice', '-c', '3',
    'locate', '--basename', '--ignore-case'
]


class LocateExtension(Extension):
    def on_input(self, query: str, trigger_id: str):
        results = []

        if not query or len(query) < 3:
            return [Result(name='Keep typing your search criteria ...')]

        process = subprocess.Popen(cmd + [query], stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        output, err = process.communicate()

        if err:
            logger.error(err)
        else:
            for path in output.decode().split('\n')[:15]:
                if path:
                    results.append(Result(
                        compact=True,
                        name=path,
                        on_enter=OpenAction(path)
                    ))
            return results

        return results or [Result(
            name=err or f'Could not find any files matching "{query}"',
            on_enter=HideWindowAction())
        ]


if __name__ == '__main__':
    LocateExtension().run()
