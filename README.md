# ulauncher-locate
File and folder search, using `locate`

## Requirements

- Ulauncher 6+ ([the GitHub version](https://github.com/friday/ulauncher-locate) works for v5 also)
- `mlocate` or `findutils` (it's almost impossible not to have these).

## Icon credit
https://pixelbuddha.net/ballicons2/
https://www.smashingmagazine.com/2014/06/freebie-ballicons-2-icon-set-png-psd-svg/
